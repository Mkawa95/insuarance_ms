<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class CompaniesController extends Controller
{
    use AuthenticatesUsers {
        logout as performLogout;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    //create function
    public function create(){
        return view('companies/create');
    }
    public function data(){
        $company_data = DB::table('companies')->get();
        return view('companies/view_companies')->with('companies', $company_data);
    }
    public function save(Request $request){
        //validate user inputs
        $this->validate(request(),[
            'company_name'     => 'required|unique:companies',
            'company_location' => 'required',
            'company_category' => 'required',
            'first_name'       => 'required',
            'last_name'        => 'required',
            'customer_email'   => 'required|email|string',
            'customer_phone'   => 'required',
            'relation'         => 'required',
        ]);
        if ($request->hasFile('document')){
            $document = $request->file('document')->storeAs('documents_folder', $request->input('company_name').'_document');
            $company_data = array(
                'company_name'     => $request->input('company_name'),
                'company_location' => $request->input('company_location'),
                'company_category' => $request->input('company_category'),
                'document'         => $document,
                'date_created'     => date('Y-m-d H:i:s')
            );
            $last_company = DB::table('companies')->insertGetId($company_data);
            $request->session()->put('last_company', $last_company);
            $request->session()->put('company_name', $request->input('company_name'));
            $request->session()->put('customer_email', $request->input('customer_email'));
            $request->session()->put('first_name', $request->input('first_name'));
            $request->session()->put('last_name', $request->input('last_name'));
            $request->session()->put('customer_phone', $request->input('customer_phone'));
            $request->session()->put('relation', $request->input('relation'));
            $request->session()->put('profile' , $request->input('profile'));
            return CompaniesController::createCustomer($request);
        }else{
            $company_data = array(
                'company_name'     => $request->input('company_name'),
                'company_location' => $request->input('company_location'),
                'company_category' => $request->input('company_category'),
                'document'         => null,
                'date_created'     => date('Y-m-d H:i:s')
            );
            $last_company = DB::table('companies')->insertGetId($company_data);
            $request->session()->put('last_company', $last_company);
            $request->session()->put('company_name', $request->input('company_name'));
            $request->session()->put('customer_email', $request->input('customer_email'));
            $request->session()->put('first_name', $request->input('first_name'));
            $request->session()->put('last_name', $request->input('last_name'));
            $request->session()->put('customer_phone', $request->input('customer_phone'));
            $request->session()->put('relation', $request->input('relation'));
            $request->session()->put('profile' , $request->input('profile'));
            return CompaniesController::createCustomer($request);
        }

    }
    public function createCustomer(Request $request){
        if ($request->hasFile('profile')){
            $profile_picture = $request->file('profile')->storeAs('customer_photos', $request->session()->get('first_name').'_profile');
            $customer_data = array(
                'company_id' => $request->session()->get('last_company'),
                'customer_first_name' => $request->session()->get('first_name'),
                'customer_last_name' => $request->session()->get('last_name'),
                'customer_phone' => $request->session()->get('customer_phone'),
                'customer_email' => $request->session()->get('customer_email'),
                'relation' => $request->session()->get('relation'),
                'profile_url' => $profile_picture
            );
            $last_customer = DB::table('customers')->insertGetId($customer_data);
            $request->session()->put('last_customer', $last_customer);
            return CompaniesController::data();
        }else{
            $customer_data = array(
                'company_id' => $request->session()->get('last_company'),
                'customer_first_name' => $request->session()->get('first_name'),
                'customer_last_name' => $request->session()->get('last_name'),
                'customer_phone' => $request->session()->get('customer_phone'),
                'customer_email' => $request->session()->get('customer_email'),
                'relation' => $request->session()->get('relation'),
                'profile_url' => null
            );
            $last_customer = DB::table('customers')->insertGetId($customer_data);
            $request->session()->put('last_customer', $last_customer);
            return CompaniesController::data();
        }
    }
    public function viewCompanyDetails(){
        $company = $_GET['company'];
        $company_query = DB::table('companies')
            ->join('customers', 'companies.company_id','=','customers.company_id')
            ->where('companies.company_id', $company)
            ->first();
        return view('companies/company_details')->with('company', $company_query);
    }
    public function downloadDocuments(){
        //nitaparudia apa ngja nipumzke kdgo
        $document = $_GET['doc'];
        return Storage::download($document, 'document_'.time());
    }
    public function deleteCompany(){
        if (isset($_POST['delete'])){
            $company = $_GET['company'];
            DB::table('companies')->where('company_id', $company)->delete();
            return redirect()->to('companies');
        }else{
            return false;
        }

    }
}
