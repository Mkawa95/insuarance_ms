@extends('include_pages/header')
@section('content')
<div class="container" style="margin-top: 100px">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="panel">
                <div class="panel-heading">PleaseLogin</div>

                <div class="panel-body">
                    <form class="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="col-sm-12 form-group offset-1 danger">
                            @if ($errors->has('email'))
                                <span class=" alert alert-danger alert-dismissible fade show">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </span>
                            @endif
                        </div>

                        <div class="col-sm-12 form-group">
                            <label for="email"><strong>E-Mail Address:</strong></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-at"></i></span>
                                </div>
                                <input id="email" type="email" style="height: 30px; font-size: small" class="form-control" name="email" value="{{ old('email') }}" required placeholder="email address">

                            </div>
                        </div>

                        <div class="col-sm-12 {{ $errors->has('password') ? ' has-error' : '' }}" style="margin-top: 15px">
                            <label for="password"><strong>Password:</strong></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa fa-at"></i></span>
                                </div>
                                <input id="password" type="password" style="height: 30px; font-size: small" class="form-control" name="password" required placeholder="password">
                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            </div>
                        </div>

                        <div class="row" style="padding: 10px 10px 10px 0;">
                            <div class="col-md-9">
                                <div class="col-sm-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <a class="btn-link" href="{{ route('password.request') }}">
                                        Forgot Password?
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding: 10px 10px 10px 0;">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" style="float: right; margin-bottom: 10px"><i class="fa fa-key"></i>Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
