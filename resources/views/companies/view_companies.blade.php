@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11 offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">Companies</div>
                            <div class="col-sm-6">
                                <a href="{{ url('companies/create') }}">
                                <button class="btn btn-success" style="float: right; padding: 5px; font-size: small">
                                    <i class="fa fa-plus"></i>  Add Company
                                </button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="">
                            <table  class="table table-striped table-bordered table-sm" id="companies" style="font-size: small">
                                <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Company Name</th>
                                    <th>Category</th>
                                    <th>Location</th>
                                    <th>Document</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                 <?php $num = 1?>
                                @foreach($companies as $company)

                                <tr>
                                    <td>{{ $num++ }}</td>
                                    <td>{{ $company->company_name }}</td>
                                    <td>{{ $company->company_category }}</td>
                                    <td>{{ $company->company_location }}</td>
                                    <td>
                                        @if($company->document == null)
                                            No document uploaded.
                                        @else
                                            <a href="{{ url('companies/download') }}?doc={{ $company->document }}">Download <i class="fa fa-download"></i></a>
                                        @endif
                                    </td>
                                    <td>

                                            <button class="btn btn-danger btn-xs" style="padding: 2px 5px 2px 5px; font-size: small"><i class="fa fa-trash"></i></button>
                                            <button class="btn btn-warning btn-xs" style="padding: 2px 5px 2px 5px; font-size: small"><i class="fa fa-edit"></i></button>
                                        <a href="{{ url('companies/details') }}?company={{ $company->company_id }}">
                                            <button class="btn btn-info btn-xs" style="padding: 2px 5px 2px 5px; font-size: small">view</button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready( function () {
            $('#companies').DataTable();
        } );
    </script>
@endpush

@section('sidebar')
    <div class="col-md-2 aside" style="padding: 0">
        <aside style="margin-top: 60px">
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                <h5 class="category-head"><strong>Dahabu Saidi</strong></h5>
                <ul class="sidebar-menu" style="margin-top: 5px">

                    <li class="sub-menu">
                        <a href="{{ url('/') }}" class="nav-link">
                            <span><i class="fa fa-angle-double-right"></i> Home</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#">
                            <span><i class="fa fa-angle-double-right"></i> Policies</span>
                        </a>
                    </li>

                    <li class="sub-menu active">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Companies</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Staffs</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Python</span>
                        </a>
                    </li>

                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
    </div>
@endsection