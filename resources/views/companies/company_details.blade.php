@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-11 offset-1">
                <div class="panel panel-default" style="border-top: 1px solid #c1c5cc">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6 company_name">
                                @if($company)
                                    {{ $company->company_name }}
                                    @endif
                            </div>
                            <div class="col-sm-6">

                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="detail_body">
                            <div class="company_details">
                                <h4>company details</h4>
                                @if($company)
                                <table class="table table-bordered table-sm table-striped">
                                    <tr>
                                        <td><strong>Company Name</strong></td>
                                        <td>{{ $company->company_name }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Company Category</strong></td>
                                        <td>{{ $company->company_category }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Location</strong></td>
                                        <td>{{ $company->company_location }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Document Attachment</strong></td>
                                        <td>
                                            @if($company->document == null)
                                                No document uploaded.
                                                <a href="#">upload document <i class="fa fa-upload"></i></a>
                                                @else
                                                <a href="{{ url('companies/download') }}?doc={{ $company->document }}">Click here to download attachment <i class="fa fa-download"></i></a>
                                                @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Date Registered</strong></td>
                                        <td>{{ date('M, y Y:', strtotime($company->date_created)) }}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start and end date</strong></td>
                                        <td>{{ date('M, y Y', strtotime($company->date_created)) }}
                                            <strong> to </strong>
                                            {{ date('M, y Y', strtotime($company->date_created)) }}
                                        </td>
                                    </tr>
                                </table>
                                    @endif
                            </div>

                            <div class="assets">
                                <h4>Registered Assets</h4>
                                <table class="table table-striped table-bordered table-sm">
                                    <thead>
                                    <tr>
                                        <td>#</td>
                                        <td>Asset Name</td>
                                        <td>Features</td>
                                        <td>Principle</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>School Bus</td>
                                        <td>Car type: SCANIA, vehicle number: DSA T466 </td>
                                        <td>{{ number_format(200000, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Renting House</td>
                                        <td>House type: msonge, House number: TBT/SG/DSM 466, Location: tabata</td>
                                        <td>{{ number_format(42200000, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Personal Computer</td>
                                        <td>Brand: SAMSUNG, serial number: 5224DSAT46644TGG, storage: 2T </td>
                                        <td>{{ number_format(1500000, 2) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="customer">
                                <h4>Customer Details</h4>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="image-box">
                                            @if($company->profile_url == null)
                                                <img src="{{ asset('images/avatar_square.png') }}" alt="Los Angeles"  width=100% class="img-responsive">
                                                <a href="#"><i class="fa fa-upload"></i> upload photo</a>
                                                @else
                                                <img src="{{ asset('storage/'.$company->profile_url) }}" alt="Los Angeles"  width=100% class="img-responsive">
                                                <a href="#"><i class="fa fa-edit"></i> change photo</a>

                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <h4 style="color: #616A6B">General Customer Contact Details</h4>
                                        <table class="table table-bordered">
                                            @if($company)
                                            <tr>
                                                <td>Names</td>
                                                <td style="text-transform: capitalize">{{ $company->customer_first_name }} {{ $company->customer_last_name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Relation</td>
                                                <td>{{ $company->relation }}</td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td style="color: #2E86C1">{{ $company->customer_email }}
                                                    <a class="#" href="#" style="float: right">
                                                        <i class="fa fa-envelope"></i> send email
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td> {{ $company->customer_phone }}

                                                </td>
                                            </tr>
                                            @endif
                                        </table>

                                        <button type="button" class="btn btn-danger" style="float: right; padding: 5px; font-size: small"
                                                data-toggle="modal" data-target="#deleteCompany{{ $company->company_id }}">
                                            <i class="fa fa-trash"></i> Delete company
                                        </button>

                                        <button class="btn btn-success" style="float: right; padding: 5px; font-size: small; margin-right: 2px">
                                            <i class="fa fa-edit"></i> Edit Information
                                        </button>

                                        <!-- The Modal -->
                                        <div class="modal fade" id="deleteCompany{{ $company->company_id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <!-- Modal Header -->
                                                    <div class="modal-header modal-head">
                                                        <h4 class="modal-title home-head"><strong style="color: cornflowerblue">Delete Company</strong></h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>

                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-sm-12">

                                                                <form  action="{{ 'delete' }}?company={{ $company->company_id }}" enctype="multipart/form-data" name="form" method="post">
                                                                    {{ csrf_field() }}
                                                                    <label for="title" ><strong></strong></label>
                                                                    <div class="form-group input-group">
                                                                        <p style=" font-weight: bold">Are you sure you want to delete {{ $company->company_name }} ?</p>
                                                                    </div>
                                                                    <button type="submit" name="delete" class="btn btn-danger" style="float: right; padding: 5px; font-size: small; margin-right: 2px">
                                                                        <i class="fa fa-trash"></i> Yes...Delete
                                                                    </button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div class="col-md-2 aside" style="padding: 0">
        <aside style="margin-top: 60px">
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                <h5 class="category-head"><strong>Dahabu Saidi</strong></h5>
                <ul class="sidebar-menu" style="margin-top: 5px">

                    <li class="sub-menu">
                        <a href="{{ url('/') }}" class="nav-link">
                            <span><i class="fa fa-angle-double-right"></i> Home</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#">
                            <span><i class="fa fa-angle-double-right"></i> Policies</span>
                        </a>
                    </li>

                    <li class="sub-menu active">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Companies</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Staffs</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Python</span>
                        </a>
                    </li>

                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
    </div>
@endsection