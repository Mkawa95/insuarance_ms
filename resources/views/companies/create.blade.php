@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-11 offset-1">
            <div class="col-sm-9 offset-md-2">
                <div class="panel" style="padding: 10px">

                    <!-- Modal Header -->
                    <div class="">
                        <h4 class="register-head" style="padding-top: 20px"><strong>Please fill the form below to register company</strong></h4>
                    </div>

                    <!-- Modal body -->
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="{{ url('companies/save') }}" name="register-form" method="POST" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    {{--Displying errors--}}

                                    <label for="pass1" class="sub-head"><strong>Company/Organization details:</strong></label>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('company_name'))<span class="has-error"><small>{{ $errors->first('company_name') }}</small></span>@endif
                                            </h5>
                                            <label for="company_name"><strong>Company name:</strong></label>
                                            <div class="input-group">
                                                <input type="text" name="company_name" id="company_name" class="form-control" value="{{ old('company_name') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('company_location'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('company_location') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="company_location"><strong>Company Location:</strong></label>
                                            <div class="input-group">
                                                <input type="text" name="company_location" id="company_location" class="form-control" value="{{ old('company_location') }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('company_category'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('company_category') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="company_category"><strong>Company category:</strong></label>
                                            <div class="input-group">
                                                <select id="company_category" class="form-control" name="company_category">
                                                    <option value="" selected>--select category--</option>
                                                    <option value="Hardware">Hardware</option>
                                                    <option value="Hardware">Hospital</option>
                                                    <option value="Hardware">School</option>
                                                    <option value="Hardware">Transport</option>
                                                    <option value="Hardware">Electronics</option>
                                                </select>
                                            </div>


                                        </div>
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('document'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('document') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="document"><strong>Company Document:</strong></label>
                                            <div class="input-group">
                                                <input type="file" name="document" id="document" class="form-control" style="padding: 2px" value="{{ old('document') }}">
                                            </div>
                                        </div>
                                    </div>

                                    <label for="pass1" class="sub-head" style="margin-top: 15px"><strong style="color: chocolate">Add Company Assets/Insurance assets:</strong></label>
                                    <div class="row asset_row" id="dynamic_field" style="background-color: whitesmoke">
                                        <div class="col-md-12" >
                                            <div class="input-group" style="padding: 0">
                                                <input type="text" name="name[]" placeholder="Asset Name" class="form-control name_list">
                                                <div class="input-group-append">
                                                <span class="input-group-text" style="padding: 3px 15px 3px 15px">
                                                    <i id="add" class="fa fa-plus"></i>
                                                </span>
                                                </div>
                                                <div class="input-group" style="" id="add_feature">
                                                    <input type="text" name="asset_feature[]" placeholder="asset feature" class="form-control">
                                                    <input type="text" name="asset_value[]" placeholder="value" class="form-control">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <button type="button" class="btn btn-success" id="add_feature" style="padding: 3px; font-size: small; border-radius: 0">
                                                                <i class="fa fa-plus-circle"> Add new feature</i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <label for="pass1" class="sub-head" style="margin-top: 15px"><strong>Customer Contact Information:</strong></label>
                                    <h5 style="font-size: small">
                                        @if($errors->has('first_name') )<span class="has-error"><small>{{ $errors->first('first_name') }}</small></span>@endif
                                    </h5>
                                    <label for="customer_name" ><strong>Customer Names:</strong></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ old('first_name') }}">
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}">
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if ($errors->has('customer_email'))<span class=" has-error"><strong>{{ $errors->first('customer_email') }}</strong></span>@endif
                                            </h5>
                                            <label for="customer_email"><strong>Email-Address:</strong></label>
                                            <div class="input-group">
                                                <input type="email" name="customer_email" id="customer_email" class="form-control" placeholder="customer@email.com" value="{{ old('customer_email') }}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('customer_phone'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('customer_phone') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="customer_phone"><strong>Phone Number:</strong></label>
                                            <div class="input-group">
                                                <input type="text" name="customer_phone" id="customer_phone" class="form-control" placeholder="0717495198" value="{{ old('customer_phone') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('relation'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('relation') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="relation"><strong>Customer relation with company:</strong></label>
                                            <div class="input-group">
                                                <select id="relation" class="form-control" name="relation">
                                                    <option value="" selected>--select relation--</option>
                                                    <option name="ceo" value="ceo">CEO</option>
                                                    <option name="hr" value="human resource">Human Resource Manager</option>
                                                    <option name="opm" value="operational manager">Operational Manager</option>
                                                    <option name="itd" value="It director">IT Director</option>
                                                    <option name="pg" value="programmer">Programmer</option>
                                                </select>
                                            </div>


                                        </div>
                                        <div class="col-sm-6">
                                            <h5 style="font-size: small">
                                                @if($errors->has('profile'))
                                                    <span class="has-error">
                                            <small>{{ $errors->first('profile') }}</small>
                                        </span>
                                                @endif
                                            </h5>
                                            <label for="profile"><strong>Customer Profile Picture:</strong></label>
                                            <div class="input-group">
                                                <input type="file" name="profile" id="profile" class="form-control" style="padding: 2px" value="{{ old('profile') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary btn-xs" name="login" type="submit" style="background-color: #2874A6; float: right; margin-top: 5px; padding: 5px; font-size: small">
                                        Register Company  </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            var i=1;
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append(
                    '<div class="col-md-12" id="row'+i+'" style="margin-top: 10px;">' +
                    '<div class="input-group" style="background-color: whitesmoke; padding: 0">' +
                    '<input type="text" name="name[]" placeholder="Asset Name" class="form-control name_list" />' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text" style="padding: 0">'+
                    '<button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style="padding: 0 15px 0 15px; border-radius: 0">' +
                    '<i id="add" class="fa fa-minus">' + '</i>' +
                    '</button>' +
                    '</span>'+
                    '</div>' +
                    '<div class="input-group">' +
                    '<input type="text" name="asset_feature[]" placeholder="Asset Feature" class="form-control name_list" />' +
                    '<input type="text" name="feature_value[]" placeholder="Feature Value" class="form-control name_list" />' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text" style="padding: 0">'+
                    '<button type="button" name="add_feature" id="'+i+'" class="btn btn-success btn_add_feature" style="padding: 3px; border-radius: 0; font-size: small">' +
                    '<i id="add" class="fa fa-plus-circle"> ' + '</i> ' + ' Add ew feature' +
                    '</button>' +
                    '</div>'+
                    '</div>'+
                    '</div>' +
                    '</div>'
                );
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
            $('#submit').click(function(){
                $.ajax({
                    url:"name.php",
                    method:"POST",
                    data:$('#add_name').serialize(),
                    success:function(data)
                    {
                        alert(data);
                        $('#add_name')[0].reset();
                    }
                });
            });
        });
    </script>

    <script>
        $(document).ready(function(){
            var i=1;
            $('#add_feature').click(function(){
                i++;
                $('#dynamic_field').append(
                    '<div class="col-md-12" id="row'+i+'">' +
                    '<div class="input-group" style="background-color: white; padding: 0">' +
                    '<input type="text" name="asset_feature[]" placeholder="Asset Feature" class="form-control name_list" />' +
                    '<input type="text" name="feature_value[]" placeholder="Feature Value" class="form-control name_list" />' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text" style="padding: 0">'+
                    '<button type="button" name="add_feature" id="'+i+'" class="btn btn-success btn_add_feature" style="padding: 3px; border-radius: 0; font-size: small">' +
                    '<i id="add" class="fa fa-plus-circle"> ' + '</i> ' + ' Add ew feature' +
                    '</button>' +
                    '</div>'+
                    '</div>' +
                    '</div>'
                );
            });
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id");
                $('#row'+button_id+'').remove();
            });
            $('#submit').click(function(){
                $.ajax({
                    url:"name.php",
                    method:"POST",
                    data:$('#add_name').serialize(),
                    success:function(data)
                    {
                        alert(data);
                        $('#add_name')[0].reset();
                    }
                });
            });
        });
    </script>
@endpush

@section('sidebar')
    <div class="col-md-2 aside" style="padding: 0">
        <aside style="margin-top: 60px">
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                <h5 class="category-head"><strong>Dahabu Saidi</strong></h5>
                <ul class="sidebar-menu" style="margin-top: 5px">

                    <li class="sub-menu">
                        <a href="{{ url('/') }}" class="nav-link">
                            <span><i class="fa fa-angle-double-right"></i> Home</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#">
                            <span><i class="fa fa-angle-double-right"></i> Policies</span>
                        </a>
                    </li>

                    <li class="sub-menu active">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Companies</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Staffs</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-angle-double-right"></i> Python</span>
                        </a>
                    </li>

                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
    </div>
@endsection