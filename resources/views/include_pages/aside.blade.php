<?php
/**
 * Created by PhpStorm.
 * User: mkawa
 * Date: 10/30/18
 * Time: 5:02 PM
 */
?>
@extends('layouts.app')
@section('content')
    <div class="col-sm-3">
        <aside>
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                <h5 class="category-head"><strong>Top Categories</strong></h5>
                <ul class="sidebar-menu" style="margin-top: 5px">

                    <li class="sub-menu">
                        <a href="" class="nav-link">
                            <span><i class="fa fa-caret-right"></i>Php Programing</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#">
                            <span><i class="fa fa-caret-right"></i>Java</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>Web development</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>Android development</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>Python</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>JavaScript</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>Database systems</span>
                        </a>
                    </li>

                    <li class="sub-menu">
                        <a href="#" class="">
                            <span><i class="fa fa-caret-right"></i>Networking</span>
                        </a>
                    </li>
                </ul>

                <ul class="sidebar-menu">
                    <li class="sub">
                        <a class="" href="#"><span>Settings</span></a>
                    </li>

                    <li class="">
                        <a class="" href="#"><span>Logout</span></a>
                    </li>
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
    </div>
@endsection
