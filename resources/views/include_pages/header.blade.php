<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my-sheet.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body style="background-color: whitesmoke">
<div class="container-fluid">
    <div id="app">
        <nav class="navbar navbar-expand-md navigation fixed-top">
            <a class="navbar-brand" href="#" style="font-size: medium; font-family: 'Noto Sans Georgian'">WICKAMA INSURANCE COMPANY LIMITED</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="fa fa-navicon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                @if (Auth::guest())
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("signup/create") }}"><i class="fa fa-user-o"></i> Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-lock"></i> Login</a>
                        </li>
                    </ul>
                @else
                    <div class="dropdown ml-auto">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ action("Auth\RegisterController@register") }}">
                            <img src="{{ asset('images/user-icon.png') }}" alt="Los Angeles"  width=30 height="30" class="img-responsive" style="border-radius: 360px">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><i class="fa fa-user-circle"></i> User Profile</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </nav>
    </div>
    @yield('content')
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>