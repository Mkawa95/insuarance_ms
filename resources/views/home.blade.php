@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11 offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-dashboard"></i> Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class=""></div>
                    You are logged in!
                    You are logged in!
                    You are logged in!
                    You are logged in!
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('sidebar')
    <div class="col-md-2 aside" style="padding: 0">
    <aside style="margin-top: 60px">
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <h5 class="category-head"><strong>Dahabu Saidi</strong></h5>
            <ul class="sidebar-menu" style="margin-top: 5px">

                <li class="sub-menu active">
                    <a href="{{ url('/') }}" class="nav-link">
                        <span><i class="fa fa-angle-double-right"></i> Home</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="#">
                        <span><i class="fa fa-angle-double-right"></i> Policies</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="{{ url('companies') }}" class="">
                        <span><i class="fa fa-angle-double-right"></i> Companies</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="#" class="">
                        <span><i class="fa fa-angle-double-right"></i> Staffs</span>
                    </a>
                </li>

                <li class="sub-menu">
                    <a href="#" class="">
                        <span><i class="fa fa-angle-double-right"></i> Python</span>
                    </a>
                </li>

            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    </div>
    @endsection