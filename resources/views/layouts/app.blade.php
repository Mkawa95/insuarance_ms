<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my-sheet.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link href="{{ asset('nice/css/widgets.css') }}" rel="stylesheet">
    <link href="{{ asset('dist/css/sb-admin-2.css') }}" rel="stylesheet">
    {{--<link href="{{ asset('nice/css/style.css') }}" rel="stylesheet">--}}
    <link href="{{ asset('nice/css/style-responsive.css') }}" rel="stylesheet" />
    <link href="{{ asset('nice/css/xcharts.min.css') }}" rel=" stylesheet">
    <link href="{{ asset('nice/css/jquery-ui-1.10.4.min.css') }}" rel="stylesheet">

    {{--<link href="{{ asset('DataTables/css/dataTables.bootstrap.min.css') }}" rel=" stylesheet">--}}
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
</head>
<body style="background-color: whitesmoke">
<div class="container-fluid">
    <div id="app">
        <nav class="navbar navbar-expand-md navigation fixed-top">
            <a class="navbar-brand" href="#" style="font-size: medium; font-family: 'Noto Sans Georgian'">WICKAMA INSURANCE COMPANY LIMITED</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="fa fa-navicon"></span>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                @if (Auth::guest())
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url("signup/create") }}"><i class="fa fa-user-o"></i> Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-lock"></i> Login</a>
                        </li>
                    </ul>
                @else
                    <div class="dropdown ml-auto">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="{{ action("Auth\RegisterController@register") }}">
                            <img src="{{ asset('images/user-icon.png') }}" alt="Los Angeles"  width=30 height="30" class="img-responsive" style="border-radius: 360px">
                            {{ Auth::user()->name }}
                        </a>
                        <div class="dropdown-menu">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><i class="fa fa-user-circle"></i> User Profile</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                @endif
            </div>
        </nav>
    </div>
    <div class="row">

            @yield('sidebar')


        <div class="col-md-9">
            @yield('content')
        </div>

    </div>
</div>
    <!-- Scripts -->

{{--<script src="{{ asset('nice/js/jquery.js')}}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery-ui-1.10.4.min.js')}}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery-1.8.3.min.js') }}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery-ui-1.9.2.custom.min.js') }}" type="text/javascript" ></script>--}}
{{--<!-- bootstrap -->--}}
{{--<script src="{{ asset('nice/js/bootstrap.min.js') }}"></script>--}}
{{--<!-- nice scroll -->--}}
{{--<script src="{{ asset('nice/js/jquery.scrollTo.min.js') }}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery.nicescroll.js" type="text/javascript') }}"></script>--}}
{{--<!-- charts scripts -->--}}
{{--<script src="{{ asset('nice/assets/jquery-knob/js/jquery.knob.js') }}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery.sparkline.js" type="text/javascript') }}"></script>--}}
{{--<script src="{{ asset('nice/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js') }}"></script>--}}
{{--<script src="{{ asset('nice/js/owl.carousel.js') }}"></script>--}}
{{--<!-- jQuery full calendar -->--}}
{{--<script src="{{ asset('nice/js/fullcalendar.min.js') }}"></script>--}}
{{--<!-- Full Google Calendar - Calendar -->--}}
{{--<script src="{{ asset('nice/assets/fullcalendar/fullcalendar/fullcalendar.js') }}"></script>--}}
{{--<!--script for this page only-->--}}
{{--<script src="{{ asset('nice/js/calendar-custom.js') }}"></script>--}}
{{--<script src="{{ asset('nice/js/jquery.rateit.min.js') }}"></script>--}}
{{--<!-- custom select -->--}}
{{--<script src="{{ asset('nice/js/jquery.customSelect.min.js') }}"></script>--}}
{{--<script src="{{ asset('nice/assets/chart-master/Chart.js') }}"></script>--}}

<!--custome script for all page-->
<script src="{{ asset('nice/js/scripts.js') }}"></script>
<script src="{{ asset('DataTables/js/dataTables.min.js') }}"></script>
<script src="{{ asset('DataTables/js/jquery.dataTables.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

{{--<script href="https://cdn.datatables.net/1.10.17/js/jquery.dataTables.min.js"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

@stack('scripts')
</body>
</html>
