<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//navigation pages
Route::get('/', 'HomeController@index');
Route::post('/', 'Auth\RegisterController@home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//company routes
Route::group(['prefix' => 'companies'], function (){
    Route::get('create', 'CompaniesController@create');
    Route::get('/', 'CompaniesController@data');
    Route::post('save', 'CompaniesController@save');
    Route::get('details', 'CompaniesController@viewCompanyDetails');
    Route::get('download', 'CompaniesController@downloadDocuments');
    Route::post('delete', 'CompaniesController@deleteCompany');
});